import java.util.Scanner;
public class Microwave{
	Scanner keyboard = new Scanner(System.in);
	
	private double size;
	private double cost;
	private int powerLevel;
	private double repairCost;
	
	public void microwaving(){
		System.out.println("BRRRRRRRRRRRRRRRRRRRRRR! This microwave uses " + powerLevel + " watts."); 
	}
	public void cost(){
		System.out.println("This microwave costs " + cost + " dollars. Thats not very cheap huh.");
	}
	
	
	//refactored code    
	public double getRepairCost(){
		return this.cost;
	}
	
	public void setValueOfRepairCost(double repairCost){
		double validValue = validate(repairCost);
		this.cost += validValue;
		this.repairCost = this.cost/4;
	}
	
	public double validate(double checkIfValid){
		if(checkIfValid > 0){
			return checkIfValid;
		} else {
			System.out.println("That wasn't a valid answer. Please input again.");
			checkIfValid = keyboard.nextInt();
			validate(checkIfValid);
		}
		return checkIfValid;
	}
	
	
	
	
	//getters and setters for fields
	public double getSize(){
		return this.size;
	}
	public void setSize(double newSize){
		this.size = newSize;		
	}
	
	public double getCost(){
		return this.cost;
	}
	public void setCost(double newCost){
		this.cost = newCost;
	}
	
	public int getPowerLevel(){
		return this.powerLevel;
	}
	public void setPowerLevel(int newPowerLevel){
		this.powerLevel = newPowerLevel;
	}

	//getter and setter for repair cost field is already done in the refactored code
	
	//constructor
	public Microwave(double size, double cost, int powerLevel){
		this.size = size;
		this.cost = cost;
		this.repairCost = cost;
		this.powerLevel = powerLevel;
	}
}

