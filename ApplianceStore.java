import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[]args){
		Scanner keyboard = new Scanner(System.in);
		Microwave[] appliances = new Microwave[4];
		
		for(int i = 0; i < appliances.length; i++){
			
			System.out.println("What size is your microwave?");
			double sizeOfMicrowave = keyboard.nextDouble();
			//appliances[i].setSize(sizeOfMicrowave);
			
			System.out.println("How much does it cost?");
			double totalCost = keyboard.nextDouble(); 
			//appliances[i].setValueOfRepairCost(totalCost); //this sets the value of both cost and repairCost
			
			System.out.println("How many watts does it use?");
			int powerLevelOfMicrowave = keyboard.nextInt();
			//appliances[i].setPowerLevel(powerLevelOfMicrowave);
			
			appliances[i] = new Microwave(sizeOfMicrowave,totalCost,powerLevelOfMicrowave);
		}
		/*
		System.out.println(appliances[3].size);
		System.out.println(appliances[3].cost);
		System.out.println(appliances[3].powerLevel); 
		
		
		appliances[0].microwaving();
		appliances[0].cost();
		*/
		
		
		//part 1 of graded lab 4 
		//appliances[1].setValueOfRepairCost(59.2);
		//System.out.println(appliances[1].getRepairCost());
		
		//part 3 of the graded lab 4
		System.out.println("Please input new values for the last microwave in the array");
		double newestSize = keyboard.nextDouble();
		double newestCost = keyboard.nextDouble();
		int newestPowerLevel = keyboard.nextInt();
		
		//printing before
		System.out.println(appliances[3].getSize());
		System.out.println(appliances[3].getRepairCost());
		System.out.println(appliances[3].getPowerLevel());
		
		//setting the values
		appliances[3].setSize(newestSize);
		appliances[3].setCost(newestCost);
		appliances[3].setPowerLevel(newestPowerLevel);
		
		//printing after
		System.out.println(appliances[3].getSize());
		System.out.println(appliances[3].getRepairCost());
		System.out.println(appliances[3].getPowerLevel());
	}
}